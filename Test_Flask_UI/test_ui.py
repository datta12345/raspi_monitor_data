from flask import Flask, render_template, request
from datetime import datetime
import subprocess

app = Flask(__name__)

def getData():
	t_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	temp=(subprocess.check_output(['vcgencmd','measure_temp']))
	temp=str(temp)
	temp=temp.replace("temp=","")[0:-3]
	v_now = (subprocess.check_output(['vcgencmd','measure_volts','core']))
	v_now = v_now.replace("volt=","")[0:-2]
	print(v_now)
	print(temp)
	hum = 91 #insert code for humidity
	print(hum)
	return t_now, temp, hum, v_now


# main route
@app.route("/")
def index():
	time, temp, hum, volts = getData()
	templateData = {
		'time': time,
		'temp': temp,
		'hum': hum,
		'voltage': volts
	}
	return render_template('index.html', **templateData)

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=80, debug=True)

